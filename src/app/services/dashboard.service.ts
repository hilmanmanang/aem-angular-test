import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TEST_API } from '../app.constant';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getDashboardData(): Observable<any> {
    return this.httpClient.get<any>(`${TEST_API}/dashboard`)
  }
}

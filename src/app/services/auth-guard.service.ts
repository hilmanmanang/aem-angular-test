import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './login.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService {

    constructor(
        private router: Router,
		private loginService: LoginService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        if (state.url === '/' && this.loginService.isAuthenticated()) return true;
        else {
            if (this.loginService.isAuthenticated()) return true;
            else {
                this.router.navigate(['/login']);
                return false;
            }
        }
	}
}

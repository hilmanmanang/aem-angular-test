import { Injectable } from '@angular/core';
import { TEST_API } from '../app.constant';
import { HttpClient } from '@angular/common/http';
import { Login } from '../model/login.model';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(
        private httpClient: HttpClient,
        private router: Router,
    ) { }

    login(loginModel: Login): Observable<any> {
        return this.httpClient.post<any>(`${TEST_API}/account/login`, loginModel);
    }

    isAuthenticated(): boolean {
        return sessionStorage.getItem('aem-jwt-token') ? true : false;
    }

    logout() {
        sessionStorage.removeItem('aem-jwt-token');
        this.router.navigate(['/login']);
    }
}

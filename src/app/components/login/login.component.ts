import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Login } from 'src/app/model/login.model';
import { LoginService } from 'src/app/services/login.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginModel: Login = new Login();
    submitLoginLoader: boolean = false;
    invalidPasswordEmail: boolean = false;

    constructor(
        private loginService: LoginService,
        private router: Router,
    ) { }

    ngOnInit(): void {
    }

    submitLogin() {
        this.submitLoginLoader = true;
        this.loginService.login(this.loginModel).subscribe({
            next: value => {
                this.submitLoginLoader = false;
                this.invalidPasswordEmail = false;
                sessionStorage.setItem('aem-jwt-token', value);
                this.router.navigate(['/']);
            },
            error: error => {
                this.submitLoginLoader = false;
                this.invalidPasswordEmail = true;
            }
        })
    }

}

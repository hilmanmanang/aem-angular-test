import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { AuthGuardService } from '../services/auth-guard.service';

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: '',
        component: DashboardComponent,
        canActivate: [AuthGuardService]
    }
]

@NgModule({
    declarations: [
        LoginComponent,
        DashboardComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forRoot(routes)
    ],
    providers: [
        LoginService
    ]
})
export class ComponentsModule { }

import { Component, OnInit } from '@angular/core';
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import * as am5 from "@amcharts/amcharts5";
import * as am5percent from "@amcharts/amcharts5/percent";
import * as am5xy from "@amcharts/amcharts5/xy";
import { DashboardService } from 'src/app/services/dashboard.service';
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    userTable: any = [];
    constructor(
        private dashboardService: DashboardService
    ) { }

    ngOnInit(): void {
        this.dashboardService.getDashboardData().subscribe({
            next: value => {
                this.userTable = value.tableUsers;

                // DONUT
                let root = am5.Root.new("chartdivDonut");
                root.setThemes([
                    am5themes_Animated.new(root)
                ]);

                let chart = root.container.children.push(
                    am5percent.PieChart.new(root, {
                        radius: am5.percent(90),
                        innerRadius: am5.percent(50)
                    })
                );

                let series = chart.series.push(
                    am5percent.PieSeries.new(root, {
                        name: "Series",
                        valueField: "value",
                        categoryField: "name"
                    })
                );

                series.data.setAll(value.chartDonut);
                series.labels.template.set("visible", true);
                series.ticks.template.set("visible", true);

                // BAR CHART
                let root2 = am5.Root.new("chartdivBar");
                root2.setThemes([
                    am5themes_Animated.new(root2)
                ]);

                let chart2 = root2.container.children.push(
                    am5xy.XYChart.new(root2, {
                        panX: false,
                        panY: false,
                        wheelX: "panX",
                        wheelY: "zoomX",
                        layout: root2.verticalLayout
                    })
                );
                let yAxis = chart2.yAxes.push(
                    am5xy.ValueAxis.new(root2, {
                        renderer: am5xy.AxisRendererY.new(root2, {
                        })
                    })
                );
                let xAxis = chart2.xAxes.push(
                    am5xy.CategoryAxis.new(root2, {
                        maxDeviation: 0.2,
                        renderer: am5xy.AxisRendererX.new(root2, {
                        }),
                        categoryField: "name"
                    })
                );
                xAxis.data.setAll(value.chartBar);
                let series1 = chart2.series.push(
                    am5xy.ColumnSeries.new(root2, {
                        xAxis: xAxis,
                        yAxis: yAxis,
                        valueYField: "value",
                        categoryXField: "name",
                        tooltip: am5.Tooltip.new(root2, {})
                    })
                );
                series1.data.setAll(value.chartBar);
            },
            error: error => {
                console.error(error)
            }
        })
    }
}
